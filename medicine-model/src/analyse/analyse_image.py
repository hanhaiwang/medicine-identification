import pickle
import matplotlib.pyplot as plt
f = open("../../resources/model_info/history.pkl", 'rb')
history = pickle.load(f)

'''
对训练的过程进行可视化展示
'''
if __name__ == '__main__':
    loss=history.history.get('loss')
    val_loss=history.history.get('val_loss')
    val_accuracy=history.history.get('val_accuracy')
    accuracy=history.history.get('accuracy')

    # plt.plot(range(len(loss)),loss,label='loss')
    # for x, y in zip(range(len(loss)), loss):
    #     plt.text(x, y, '%.2f' % y, ha='center', va='bottom', fontsize=9)
    #
    # plt.title('Training Loss')
    # plt.xlabel("epoch")
    # plt.ylabel('loss')
    # plt.legend()
    # plt.show()

    plt.plot(range(len(accuracy)),accuracy,label='accuracy')
    plt.plot(range(len(val_accuracy)),val_accuracy,label='val_accuracy')

    # for x, y in zip(range(len(accuracy)), accuracy):
    #     plt.text(x, y , '%.4f'%y, ha='center', va='bottom', fontsize=9)
    #
    # for x, y in zip(range(len(val_accuracy)), val_accuracy):
    #     plt.text(x, y , '%.4f'%y, ha='center', va='bottom', fontsize=9)

    plt.title('Training And Validation Accuracy')
    plt.xlabel("epoch")
    plt.ylabel('accuracy')
    plt.legend()
    plt.show()