import numpy as np
from tensorflow import keras

'''
模型预测
'''
model = keras.models.load_model('chinese_medicine_model.h5')
img_path = '/Users/com.xiaohao/Documents/Java/chinese-medicine-identification/medicine-dataset/dataset/丁公藤/丁公藤_1.jpg'
if __name__ == '__main__':
    img = keras.preprocessing.image.load_img(img_path)
    img = keras.preprocessing.image.img_to_array(img)
    img = np.expand_dims(img, axis=0)
    img = keras.applications.xception.preprocess_input(img)
    features = model.predict(img)[0]
    for i in reversed(sorted(features)[-10:]):
        print(np.argwhere(features == i)[0][0], ':',i, sep='', end=',')
    print()
