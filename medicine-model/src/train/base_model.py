import keras

'''
模型的构建，使用keras构建Xception模型，预先加载Imagenet已经训练好的参数进行迁移学习
本模型采用Xception的所有卷积层和池化层，最后一层加入全连接神经网络用于分类，详细请查看README
'''

img_size = (299, 299, 3)
base_model = keras.applications \
    .xception.Xception(include_top=False,
                       weights='../../resources/keras-model/'
                               'xception_weights_tf_dim_ordering_tf_kernels_notop.h5',
                       input_shape=img_size,
                       pooling='avg')
model = keras.layers.Dense(628, activation='softmax', name='predictions')(base_model.output)
model = keras.Model(base_model.input, model)
model.trainable = False
