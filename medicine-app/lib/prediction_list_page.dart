import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:medicine_app/medicine_detail_page.dart';
import 'package:medicine_app/request_util.dart';
import 'package:medicine_app/slide_page_route.dart';

class PredictionListPage extends StatefulWidget {
  final String _path;

  PredictionListPage({Key? key, required String path})
      : _path = path,
        super(key: key);

  @override
  _PredictionListPageState createState() => _PredictionListPageState();
}

class _PredictionListPageState extends State<PredictionListPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          color: Colors.black,
          icon: Icon(Icons.arrow_back_ios),
          iconSize: 18,
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        elevation: 0.4,
        toolbarHeight: 55,
        title: Text(
          "预测",
          style: TextStyle(color: Colors.black, fontSize: 17),
        ),
      ),
      body: FutureBuilder(
        future: Future.sync(() async {
          final FormData formData = FormData.fromMap({"picture": await MultipartFile.fromFile(widget._path)});
          final response = await Request.getDio().post('/medicine/prediction', data: formData);
          return response.data['data'] as Map<String, dynamic>;
        }),
        builder: (BuildContext context, AsyncSnapshot<Map<String, dynamic>> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            final entries = snapshot.data?.entries.toList().sublist(0, 14);
            final maxLength = entries?.length ?? 0;
            return ListView.separated(
              padding: EdgeInsets.symmetric(vertical: 10),
              itemBuilder: (BuildContext context, int index) {
                if (index < maxLength) {
                  return InkWell(
                    splashColor: Colors.grey.shade300,
                    onTap: () {
                      Navigator.of(context).push(SlidePageRoute(
                        builder: MedicineDetailPage(name: "${entries?[index].key}"),
                      ));
                    },
                    child: Container(
                      height: 65,
                      child: Row(
                        children: [
                          Expanded(
                              flex: 2,
                              child: Icon(
                                Icons.wallpaper_rounded,
                                color: Theme.of(context).primaryColor,
                              )),
                          Expanded(
                              flex: 3,
                              child: Text(
                                '${entries?[index].key}',
                                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                              )),
                          Expanded(flex: 3, child: Text('置信度：${((entries?[index].value) * 100).toStringAsFixed(2)}%')),
                          Expanded(
                              flex: 1,
                              child: Icon(
                                Icons.chevron_right,
                                size: 18,
                                color: Theme.of(context).primaryColor,
                              )),
                        ],
                      ),
                    ),
                  );
                } else {
                  return Center(
                    child: Text(
                      '- 没有更多了 -',
                      style: TextStyle(color: Colors.grey, height: 2.5),
                    ),
                  );
                }
              },
              itemCount: maxLength + 1,
              separatorBuilder: (BuildContext context, int index) {
                return Divider(
                    indent: 25, endIndent: 20, height: 0.5, color: Theme.of(context).primaryColor.withAlpha(80));
              },
            );
          } else {
            return Center(
                child: SpinKitFadingCircle(
              color: Theme.of(context).primaryColor,
              size: 35,
            ));
          }
        },
      ),
    );
  }
}
