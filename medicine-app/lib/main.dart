import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:medicine_app/mine_page.dart';
import 'package:medicine_app/prediction_list_page.dart';
import 'package:medicine_app/request_util.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:medicine_app/slide_page_route.dart';
import 'home_page.dart';

void main() {
  Request.initialize();
  runApp(MedicineApp());
  if (Platform.isAndroid) {
    SystemUiOverlayStyle systemUiOverlayStyle = SystemUiOverlayStyle(statusBarColor: Colors.transparent);
    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
  }
}

class MedicineApp extends StatefulWidget {
  @override
  State<MedicineApp> createState() => _MedicineAppState();
}

class _MedicineAppState extends State<MedicineApp> {
  final _pageList = [
    HomePage(),
    MinePage(),
  ];

  final _pageViewController = PageController();
  int _currentPageIndex = 0;

  void _bottomAppBarTap(index) {
    setState(() {
      _currentPageIndex = index;
    });
    _pageViewController.animateToPage(index, duration: Duration(milliseconds: 300), curve: Curves.ease);
  }

  Color _bottomAppBarItemColor(int index, BuildContext context) {
    return _currentPageIndex == index ? Theme.of(context).selectedRowColor : Theme.of(context).unselectedWidgetColor;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: '中药识别',
      home: Scaffold(
        body: PageView(
          physics: NeverScrollableScrollPhysics(),
          children: _pageList,
          controller: _pageViewController,
        ),
        floatingActionButton: Builder(
          builder: (BuildContext context) {
            return SizedBox(
              width: 48,
              height: 48,
              child: FloatingActionButton(
                backgroundColor: Theme.of(context).selectedRowColor,
                onPressed: () async {
                  final ImagePicker _picker = ImagePicker();
                  final XFile? picture =
                      await _picker.pickImage(source: ImageSource.camera, maxHeight: 299, maxWidth: 299);
                  if (picture != null) {
                    Navigator.push(
                      context,
                      SlidePageRoute(
                        builder: PredictionListPage(path: picture.path),
                      ),
                    );
                  } else {
                    Fluttertoast.showToast(
                      msg: "取消上传",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIosWeb: 1,
                      backgroundColor: Theme.of(context).unselectedWidgetColor,
                      textColor: Colors.white,
                      fontSize: 13,
                    );
                  }
                },
                child: Icon(
                  Icons.photo_camera,
                  color: Colors.white70,
                ),
              ),
            );
          },
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        bottomNavigationBar: Container(
          height: 55,
          child: BottomAppBar(
            color: Colors.grey.shade50,
            elevation: 6,
            shape: CircularNotchedRectangle(),
            child: Builder(
              builder: (BuildContext context) {
                return Row(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Expanded(
                        flex: 2,
                        child: GestureDetector(
                          onTap: () => _bottomAppBarTap(0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                _currentPageIndex == 0 ? Icons.home_rounded : Icons.home_outlined,
                                color: _bottomAppBarItemColor(0, context),
                              ),
                              Text(
                                "首页",
                                style: TextStyle(
                                    fontSize: 10,
                                    fontWeight: FontWeight.w500,
                                    color: _bottomAppBarItemColor(0, context)),
                              ),
                            ],
                          ),
                        )),
                    Expanded(flex: 1, child: Container()),
                    Expanded(
                      flex: 2,
                      child: GestureDetector(
                        onTap: () => _bottomAppBarTap(1),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              _currentPageIndex == 1 ? Icons.person_rounded : Icons.person_outline_rounded,
                              color: _bottomAppBarItemColor(1, context),
                            ),
                            Text(
                              "我的",
                              style: TextStyle(
                                  fontSize: 10, fontWeight: FontWeight.w500, color: _bottomAppBarItemColor(1, context)),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                );
              },
            ),
          ),
        ),
      ),
      theme: Theme.of(context).copyWith(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          primaryColor: Colors.deepOrange,
          primaryColorLight: Colors.white,
          primaryColorDark: Colors.black87,
          accentColor: Colors.deepOrange[400]?.withAlpha(224),
          selectedRowColor: Colors.deepOrangeAccent[700]),
    );
  }
}

bool isDebug() {
  return kDebugMode;
}
