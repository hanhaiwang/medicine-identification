import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class MinePage extends StatefulWidget {
  MinePage({Key? key}) : super(key: key);

  @override
  _MinePageState createState() => _MinePageState();
}

class _MinePageState extends State<MinePage> {
  Container _menuListItem(String title, IconData iconData, Function? callback) {
    final _textStyle =
        TextStyle(color: Theme.of(context).primaryColorDark.withAlpha(160), fontSize: 15, fontWeight: FontWeight.w500);
    return Container(
      height: 55,
      child: GestureDetector(
        onTap: () => Fluttertoast.showToast(
          msg: "开发ing",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Theme.of(context).unselectedWidgetColor,
          textColor: Colors.white,
          fontSize: 12,
        ),
        child: Row(
          children: [
            Expanded(
                flex: 1,
                child: Icon(
                  iconData,
                  size: 20,
                  color: Theme.of(context).accentColor,
                )),
            Expanded(
                flex: 5,
                child: Text(
                  title,
                  style: _textStyle,
                )),
            Expanded(
                flex: 1,
                child: Icon(
                  Icons.chevron_right,
                  color: Colors.grey.shade400,
                  size: 18,
                )),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).accentColor,
        centerTitle: true,
        elevation: 0,
        toolbarHeight: 55,
        title: Text(
          "个人中心",
          style: TextStyle(color: Colors.black, fontSize: 16),
        ),
      ),
      body: Container(
        child: Column(
          children: [
            Container(
              width: double.infinity,
              height: 150,
              alignment: Alignment.center,
              color: Theme.of(context).accentColor,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Icon(
                    Icons.account_circle_sharp,
                    size: 50,
                    color: Colors.black87,
                  ),
                  TextButton(
                    onPressed: () => {},
                    child: Text(
                      '点击登录',
                      style: TextStyle(color: Colors.black87),
                    ),
                    style: ButtonStyle(overlayColor: MaterialStateProperty.all(Colors.transparent)),
                  ),
                ],
              ),
            ),
            ListView.separated(
                padding: EdgeInsets.symmetric(horizontal: 10),
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int index) {
                  switch (index) {
                    case 0:
                      return _menuListItem('历史记录', Icons.timelapse_outlined, null);
                    case 1:
                      return _menuListItem('中药问答', Icons.question_answer, null);

                    case 2:
                      return _menuListItem('方剂智能推荐', Icons.account_tree_rounded, null);
                    case 3:
                      return _menuListItem('收藏', Icons.add_to_photos, null);
                    case 4:
                      return _menuListItem('关于', Icons.info, null);
                    case 5:
                      return _menuListItem('分享', Icons.share, null);
                    default:
                      return Container();
                  }
                },
                separatorBuilder: (BuildContext context, int index) {
                  return Divider(
                      indent: 10, endIndent: 10, height: 0.5, color: Theme.of(context).accentColor.withAlpha(80));
                },
                itemCount: 6),
          ],
        ),
      ),
    );
  }
}
