# 中药图片拍照识别App

![](https://img.shields.io/badge/release-v2.1-green.svg)
![](https://img.shields.io/badge/language-Java%2011-green.svg)
![](https://img.shields.io/badge/language-Python%203-green.svg)
![](https://img.shields.io/badge/dependency-SpringBoot-green.svg)
![](https://img.shields.io/badge/dependency-TensorFlow-green.svg)
![](https://img.shields.io/badge/license-GPL-green.svg)

[中文](README.md) | [English](README.md)


[<svg width="20" role="img" viewBox="0 0 24 24"><path d="M12 .297c-6.63 0-12 5.373-12 12 0 5.303 3.438 9.8 8.205 11.385.6.113.82-.258.82-.577 0-.285-.01-1.04-.015-2.04-3.338.724-4.042-1.61-4.042-1.61C4.422 18.07 3.633 17.7 3.633 17.7c-1.087-.744.084-.729.084-.729 1.205.084 1.838 1.236 1.838 1.236 1.07 1.835 2.809 1.305 3.495.998.108-.776.417-1.305.76-1.605-2.665-.3-5.466-1.332-5.466-5.93 0-1.31.465-2.38 1.235-3.22-.135-.303-.54-1.523.105-3.176 0 0 1.005-.322 3.3 1.23.96-.267 1.98-.399 3-.405 1.02.006 2.04.138 3 .405 2.28-1.552 3.285-1.23 3.285-1.23.645 1.653.24 2.873.12 3.176.765.84 1.23 1.91 1.23 3.22 0 4.61-2.805 5.625-5.475 5.92.42.36.81 1.096.81 2.22 0 1.606-.015 2.896-.015 3.286 0 .315.21.69.825.57C20.565 22.092 24 17.592 24 12.297c0-6.627-5.373-12-12-12"></path></svg>]() &nbsp;&nbsp;[<svg  width="22" viewBox="0 0 2000 2000"><path d="M898 1992q183 0 344-69.5t283-191.5q122-122 191.5-283t69.5-344q0-183-69.5-344T1525 477q-122-122-283-191.5T898 216q-184 0-345 69.5T270 477Q148 599 78.5 760T9 1104q0 183 69.5 344T270 1731q122 122 283 191.5t345 69.5zm199-400H448q-17 0-30.5-14t-13.5-30V932q0-89 43.5-163.5T565 649q74-45 166-45h616q17 0 30.5 14t13.5 31v111q0 16-13.5 30t-30.5 14H731q-54 0-93.5 39.5T598 937v422q0 17 14 30.5t30 13.5h416q55 0 94.5-39.5t39.5-93.5v-22q0-17-14-30.5t-31-13.5H842q-17 0-30.5-14t-13.5-31v-111q0-16 13.5-30t30.5-14h505q17 0 30.5 14t13.5 30v250q0 121-86.5 207.5T1097 1592z"/></svg>](https://gitee.com/xiaohaoo/chinese-medicine-identification-admin)

### 简介

<div style="font-size: 18px;line-height: 35px;text-indent: 2em">
<p>
中医药是中华民族的传统文化瑰宝，目前，中药种类繁多，辨认多依靠于经验，这增加了我们辨识出中药的难度；同时，我们需要记忆大量的中药特征和药效，查询效率慢。
</p>
<p>
我们目标是开发一款中药识别的APP，主要利用细粒度图像识别技术，用户只需利用手机对中药进行拍照便可识别，提高学习效率，高效的识别与学习各类中药的特征和药效价值，促进人工智能与医疗的结合。
</p>
</div>

### 预览

![图片](assets/images/启动屏.png)
![图片](assets/images/首页.png)
![图片](assets/images/筛选.png)
![图片](assets/images/中药详细信息.png)
![图片](assets/images/拍照识别.png)
![图片](assets/images/预测结果.png)
![图片](assets/images/搜索结果.png)

### 视频
- **[视频演示](https://www.bilibili.com/video/BV14T4y157rM)**

### 下载
- **[APP下载地址](https://gitee.com/xiaohaoo/chinese-medicine-identification/releases)**

### 版本说明 2.x
对版本1.x进行重构升级，重新组织了代码结构，移除了特别不稳定且重量级的组件Deeplearn4j。

### 开发计划
使用Flutter对移动端重写，提高性能与跨平台性，同时，进一步调高检测模型的精度。


### 交流、反馈与参与贡献
- 如需关注项目最新动态，请Watch、Star项目，同时也是对项目最好的支持
- 欢迎参与技术讨论、二次开发咨询、问题反馈和建议！
- QQ：993021993
- 微信：

  ![](assets/images/WeChat.png)

  感谢！^_^


![访客统计](https://visitor-badge.glitch.me/badge?page_id=chinese-medicine-identification)


