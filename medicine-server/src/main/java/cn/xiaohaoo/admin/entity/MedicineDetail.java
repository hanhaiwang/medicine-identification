package cn.xiaohaoo.admin.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.Id;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
@Entity
public class MedicineDetail {
    @Id
    private Long id;
    private String name;
    private String title;
    private String img;
    private String fullName;
    private String alias;
    private String englishName;
    private String medicinalParts;
    private String animalMorphology;
    private String originDistribution;
    private String harvestingProcessing;
    private String medicinalProperties;
    private String tropism;
    private String efficacy;
    private String clinical;
    private String pharmacology;
    private String composition;
    private String taboo;
    private String prescription;
}
