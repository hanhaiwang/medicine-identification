package cn.xiaohaoo.admin.entity;


import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.Id;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
public class Search {
    @Id
    @TableId
    private String userId;
    private String key;
}
