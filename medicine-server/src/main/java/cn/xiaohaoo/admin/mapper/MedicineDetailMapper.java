package cn.xiaohaoo.admin.mapper;

import cn.xiaohaoo.admin.entity.MedicineDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MedicineDetailMapper extends BaseMapper<MedicineDetail> {
}
